name := "monitoringTool"

version := "1.0"

scalaVersion := Common.scalaVersion

lazy val pluginApi = project

lazy val fileStatePlugin = project.dependsOn(pluginApi)

lazy val launcher = project.dependsOn(pluginApi)

lazy val app = project.dependsOn(pluginApi, fileStatePlugin)//FIXME убрать плагин из зависимости