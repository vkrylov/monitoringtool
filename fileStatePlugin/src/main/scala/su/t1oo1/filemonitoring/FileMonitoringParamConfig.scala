package su.t1oo1.filemonitoring

import java.io.File

import su.t1oo1.monitoringtool.pluginapi.{Param, ParamConfig}

/**
 * Конфигурация мониторинга состояния файла
 * 
 *
 * @param filePath путь к наблюдаемому файлу
 * @param monitoringType тип мониторинга
 * @param plugin плагин мониторинга файлов
 */
class FileMonitoringParamConfig(val filePath: String, monitoringType: MonitoringType, val plugin: FileMonitoringPlugin) extends ParamConfig {

  override def asText: String = s""""file:{filePath:"$filePath", monitoringType:"${monitoringType.name}"}""""

  override def createTask(param: Param): Runnable = {
    new Runnable {
      override def run(): Unit = {
        val f = new File(filePath)
        val result = monitoringType.check(f)
        println(result)
        param.value = Some(result)
      }
    }
  }
}
