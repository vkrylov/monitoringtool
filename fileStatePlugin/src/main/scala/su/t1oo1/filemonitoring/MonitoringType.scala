package su.t1oo1.filemonitoring

import java.io.File

/**
 * Тип мониторинга
 */
trait MonitoringType {
  def name: String

  override def toString = name

  /**
   * Проверить состояние
   *
   * @param file проверяемый файл
   * @return результат проверки
   */
  def check(file: File): String
}

/**
 * Мониторинг размера файла
 */
class FileSize extends MonitoringType {
  override def name: String = "file size"

  override def check(file: File): String = {
    if (file.exists()) {
      if (file.isFile) {
        file.length().toString
      } else {
        "Cannot check directory size"
      }
    } else {
      "File does not exist"
    }
  }
}