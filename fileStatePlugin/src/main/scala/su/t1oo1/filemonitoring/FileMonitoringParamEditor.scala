package su.t1oo1.filemonitoring

import java.awt.{GridLayout, Label, TextField}
import javax.swing.{DefaultComboBoxModel, JComboBox}

import su.t1oo1.monitoringtool.pluginapi.{ParamConfig, ParamEditorPanel}

/**
 * Графический редактор параметров мониторинга
 *
 * @param plugin плагин мониторинга файлов
 */
class FileMonitoringParamEditor(plugin: FileMonitoringPlugin) extends ParamEditorPanel {

  setLayout(new GridLayout(2, 2, 5, 5))

  add(new Label("File"))
  private val filePath: TextField = new TextField("build.sbt")
  add(filePath)
  add(new Label("Observing type"))
  val typeComboBox = createTypeComboBox
  add(typeComboBox)

  override def config: ParamConfig = {
    new FileMonitoringParamConfig(filePath.getText, typeComboBox.getSelectedItem.asInstanceOf[MonitoringType], plugin)
  }

  private def createTypeComboBox = {
    val typeComboModel = new DefaultComboBoxModel[MonitoringType]()
    val typeCombo = new JComboBox[MonitoringType](typeComboModel)
    typeComboModel.addElement(new FileSize)
    typeCombo
  }
}