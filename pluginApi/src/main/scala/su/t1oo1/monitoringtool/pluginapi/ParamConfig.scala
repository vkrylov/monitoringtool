package su.t1oo1.monitoringtool.pluginapi

/**
 * Конфигурация параметра мониторинга
 */
trait ParamConfig {

  /**
   *
   * @return отобразить настройки как простой текст
   */
  def asText: String

  override def toString: String = asText

  /**
   * @return плагин
   */
  def plugin: Plugin

  /**
   * @param param параметр мониторинга
   * @return задание для проведения мониторинга по параметру
   */
  def createTask(param: Param): Runnable
}
