package su.t1oo1.monitoringtool.pluginapi

/**
 * Плагин проведения мониторинга
 */
trait Plugin {

  /**
   * @return простое название плагина
   */
  def name: String

  /**
   * @return графическая панель редактора, специфичная для данного плагина
   */
  def editorPanel: ParamEditorPanel

  override def toString = name
}
