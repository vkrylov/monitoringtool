package su.t1oo1.monitoringtool.pluginapi

import javax.swing.JPanel

/**
 * Графическая панель настройки параметра. Используется для создания конфигурации параметра
 */
trait ParamEditorPanel extends JPanel {

  def config: ParamConfig
}
