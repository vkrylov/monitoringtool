package su.t1oo1.monitoringtool.pluginapi

/**
 * Параметр мониторинга. Используется для создания задачи мониторинга и отображения текущего состояния
 *
 * @param name название
 * @param config конфигурация
 * @param value текущее значение
 * @param periodicity периодичность проведения мониторинга
 */
case class Param(var name: String, var config: ParamConfig, var value: Option[String], var periodicity: Int)
