package su.t1oo1.launcher

import java.io.File
import java.net.URLClassLoader

/**
 * Входная точка в приложение. Использованы отделенные загрузчики классов
 * для плагинов и графического ядра. Графическое ядро должно располагаться в каталоге binPath.
 * Загрузка плагинов происходит в PluginLoader
 */
object EntryPoint extends App {
  val guiClass = "su.t1oo1.monitoring.AppGui"
  val binPath = "bin"

  val appClass = binClassLoader.loadClass(guiClass)
  val appInstance = appClass.newInstance()
  appClass.getMethods.find(m => m.getName == "start").foreach{m =>
    m.invoke(appInstance, PluginLoader.plugins.map(_._2.instance).toList)
  }

  private def binClassLoader = {
    classLoader(binPath, getClass.getClassLoader)
  }

  private def classLoader(path: String, parent: ClassLoader) = {
    val entries = new File(path).listFiles()
    val urls = for (e <- entries) yield e.toURI.toURL
    new URLClassLoader(urls.toArray, parent)
  }
}
