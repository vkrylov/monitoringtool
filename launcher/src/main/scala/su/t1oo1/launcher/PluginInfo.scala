package su.t1oo1.launcher

import java.io.File
import java.net.URLClassLoader
import java.util.Properties
import java.util.jar.JarFile

import su.t1oo1.monitoringtool.pluginapi.Plugin

object PluginInfo {
  val propertiesFileName = "plugin.properties"
  val mainClassKey = "main.class"

  /**
   * Загрузить плагин из jar-файла. Информация о плагине должна быть описана в корне jar в файле
   * параметров и содержать ссылку на главный класс плагина. Поэтому есть возможность не привязываться
   * к структуре плагина
   *
   * @param jarFile файл плагина
   * @return информация о плагине
   */
  def apply(jarFile: File): PluginInfo = {
    println("trying to plug file in: " + jarFile)
    try {
      pluginProperties(jarFile) match {
        case None => throw new IllegalArgumentException("No props file found")
        case Some(props) =>
          val pluginClassName = props.getProperty(mainClassKey)
          if (pluginClassName == null || pluginClassName.isEmpty) {
            throw new PluginLoadException("Missing property " + mainClassKey)
          }
          val jarURL = jarFile.toURI.toURL
          val classLoader = new URLClassLoader(Array(jarURL), getClass.getClassLoader)
          val pluginClass = classLoader.loadClass(pluginClassName)
          new PluginInfo(pluginClass.newInstance().asInstanceOf[Plugin])
      }
    } catch {
      case e: Exception => throw new PluginLoadException(e.getMessage)
    }
  }

  private def pluginProperties(file: File): Option[Properties] = {
    val jar = new JarFile(file)
    val entries = jar.entries()
    var result: Option[Properties] = None

    while (entries.hasMoreElements) {
      val entry = entries.nextElement()
      if (entry.getName.equals(propertiesFileName)) {
        using(jar.getInputStream(entry)) { resource =>
          val properties = new Properties()
          properties.load(resource)
          result = Some(properties)
        }
      }
    }
    result
  }

  def using[T <: {def close()}](resource: T)(block: T => Unit) = {
    try {
      block(resource)
    } finally {
      if (resource != null) resource.close()
    }
  }
}

class PluginInfo(val instance: Plugin)

case class PluginLoadException(message: String) extends Exception(message)