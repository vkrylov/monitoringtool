package su.t1oo1.launcher

import java.io.{FileFilter, File}

/**
 * Загрузчик плагинов из jar-файлов
 */
object PluginLoader {
  val pluginDir = new File("plugins")

  val jars = pluginDir.listFiles(new FileFilter() {
    override def accept(file: File): Boolean = {
      file.isFile && file.getName.endsWith(".jar")
    }
  })

  /**
   * Загруженные плагины
   */
  val plugins = for (file <- jars) yield  {
    (file.getName, PluginInfo(file))
  }
}
