import AssemblyKeys._

name := "launcher"

scalaVersion := "2.11.4"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.2"

mainClass in assembly := Some("su.t1oo1.launcher.EntryPoint")