package su.t1oo1.monitoring

import su.t1oo1.filemonitoring.FileMonitoringPlugin
import su.t1oo1.monitoring.ui.{MainFrame, PluginEditorModel}
import su.t1oo1.monitoringtool.pluginapi.Plugin

//FIXME удалить тестовый запуск в обход главного загрузчика
object AppGui extends App {
  val a = new AppGui
  val plugins = Array[Plugin](new FileMonitoringPlugin)
  PluginEditorModel.plugins = plugins
  a.start(Nil)
}

/**
 * Точка входа в графический интерфейс приложения
 */
class AppGui {
  TaskExecutor

  /**
   * Запустить графический интерфейс
   *
   * @param plugins список плагинов
   */
  def start(plugins: List[Plugin]): Unit = {
    PluginEditorModel.plugins = plugins.toArray

    println(s"Connected plugins (${plugins.size}}):")
    PluginEditorModel.plugins.foreach(p => println("\t" + p))

    val frame = new MainFrame
    frame.setVisible(true)
  }
}