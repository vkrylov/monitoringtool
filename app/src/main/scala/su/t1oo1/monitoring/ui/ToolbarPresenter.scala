package su.t1oo1.monitoring.ui

import java.awt.Component

class ToolbarPresenter(val view: Component) {

  def onAdd(): Unit = {
    val editor = ParamEditor(view, PluginEditorModel.plugins)
    editor.setVisible(true)
  }

  def onDelete(): Unit = EventBus.fire(DeleteSelectedParam())

  def onEdit(): Unit = EventBus.fire(EditSelectedParam())

  def onPlugins(): Unit = {
    val editor = PluginEditor(view)
    editor.setVisible(true)
  }

  EventBus.addListener(new Listener[EditParam] {
    override def handle(event: EditParam): Unit = {
      val editor = ParamEditor(view, PluginEditorModel.plugins)
      editor.bind(event.param)
      editor.setVisible(true)
    }
  }, classOf[EditParam])
}
