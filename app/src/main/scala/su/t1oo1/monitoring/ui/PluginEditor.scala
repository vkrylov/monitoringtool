package su.t1oo1.monitoring.ui

import java.awt.{BorderLayout, Component}
import javax.swing._

import su.t1oo1.monitoringtool.pluginapi.Plugin

/**
 * Редактор плагинов
 */
//TODO добавить динамическую загрузку и отключение плагинов, чтобы был редактор, а не список плагинов
object PluginEditor {
  def apply(parent: Component): PluginEditor = {
    val editor = new PluginEditor(parent)
    PluginEditorModel.plugins.foreach(editor.add)
    editor
  }
}

class PluginEditor(parent: Component) extends JDialog(SwingUtilities.getWindowAncestor(parent)) {

  import su.t1oo1.monitoring.ui.UiFactory._

  setModal(true)
  setTitle("Plugins")
  setSize(300, 300)
  setLocationRelativeTo(null)

  val content = new JPanel(borderLayout)
  val pluginListModel = new DefaultListModel[Plugin]()

  val pluginList = new JList[Plugin](pluginListModel)
  content.add(pluginList, BorderLayout.CENTER)
  setContentPane(content)

  setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)

  def add(plugin: Plugin): Unit = {
    pluginListModel.addElement(plugin)
  }
}
