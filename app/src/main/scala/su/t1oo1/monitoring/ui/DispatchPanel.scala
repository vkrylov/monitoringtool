package su.t1oo1.monitoring.ui

import java.awt.BorderLayout
import javax.swing.{Box, JPanel}

import su.t1oo1.monitoring.ui.UiFactory._

/**
 * Панель запуска и остановки заданий
 */
object DispatchPanel {
  def apply(): DispatchPanel = new DispatchPanel(new DispatchPanelPresenter())
}

class DispatchPanel(presenter: DispatchPanelPresenter) extends JPanel(borderLayout) {
  val panel = Box.createHorizontalBox()
  val runButton = button("Run") { e =>
    presenter.onRun()
  }
  panel.add(runButton)
  val stopButton = button("Stop") { e =>
    presenter.onStop()
  }
  panel.add(stopButton)
  run(b = false)

  add(panel, BorderLayout.WEST)

  EventBus.addListener(new Listener[Run] {
    override def handle(event: Run): Unit = {
      run(b = true)
    }
  }, classOf[Run])

  EventBus.addListener(new Listener[Stop] {
    override def handle(event: Stop): Unit = {
      run(b = false)
    }
  }, classOf[Stop])

  def run(b: Boolean): Unit = {
    runButton.setEnabled(!b)
    stopButton.setEnabled(b)
  }
}
