package su.t1oo1.monitoring.ui

class DispatchPanelPresenter {
  def onRun(): Unit = EventBus.fire(Run())

  def onStop(): Unit = EventBus.fire(Stop())
}
