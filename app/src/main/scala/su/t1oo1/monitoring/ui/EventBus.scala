package su.t1oo1.monitoring.ui

import su.t1oo1.monitoringtool.pluginapi.Param

/**
 * Шина работы с событиями
 */
//TODO рассмотреть вариант использования шины, встроеной в swing
//TODO учесть случаи необходимости удаления слушателей событий
//FIXME найти способ убрать спагетти с дженериками
object EventBus {
  import scala.collection.mutable
  val listenerMap = mutable.Map.empty[Class[Event], List[Listener[Event]]].withDefaultValue(Nil)

  def addListener[T <: Event](listener: Listener[T], eventType: Class[T]): Unit = {
    val key: Class[Event] = eventType.asInstanceOf[Class[Event]]
    val listeners: List[Listener[Event]] = listener.asInstanceOf[Listener[Event]] :: listenerMap(key)
    listenerMap += (key -> listeners)
  }

  def fire[T <: Event](event: T): Unit = {
    val key: Class[Event] = event.getClass.asInstanceOf[Class[Event]]
    val listeners = listenerMap(key)
    listeners.foreach(l => l.handle(event))
  }
}

trait Event

trait Listener[T <: Event] {
  def handle(event: T)
}

case class AddParam(param: Param) extends Event

case class DeleteSelectedParam() extends Event

case class EditSelectedParam() extends Event

case class EditParam(param: Param) extends Event

case class UpdateParam(param: Param) extends Event

case class Run() extends Event

case class RunParamObserving(params: List[Param]) extends Event

case class Stop() extends Event
