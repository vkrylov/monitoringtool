package su.t1oo1.monitoring.ui

import su.t1oo1.monitoringtool.pluginapi.Param

trait ParamEditorPresenter {

  def onSave(param: Param): Boolean

  def onEdit(param: Param): Boolean

  def onCancel(): Unit
}
