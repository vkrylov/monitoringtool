package su.t1oo1.monitoring.ui

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{Button, BorderLayout}
import javax.swing.BorderFactory

object UiFactory {

  def borderLayout:BorderLayout = new BorderLayout(5, 5)

  def emptyBorder = BorderFactory.createEmptyBorder(5,5,5,5)

  def button(text: String)(action: ActionEvent => Unit): Button = {
    val button = new Button(text)
    button.addActionListener(new ActionListener {
      override def actionPerformed(e: ActionEvent): Unit = action(e)
    })
    button
  }
}
