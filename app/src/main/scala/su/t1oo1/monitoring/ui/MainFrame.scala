package su.t1oo1.monitoring.ui

import java.awt.{BorderLayout, Container}
import javax.swing._
import javax.swing.table.DefaultTableModel

import su.t1oo1.monitoring.TaskExecutor
import su.t1oo1.monitoringtool.pluginapi.Param

/**
 * Главное окно приложения
 */
//TODO вынести работу с таблицей в отдельную сущность
//FIXME исправить обновление информации в таблице. В т.ч. при редактировании параметров
class MainFrame extends JFrame("Monitoring tool") {

  import su.t1oo1.monitoring.ui.UiFactory._

  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  setSize(500, 400)
  setLocationRelativeTo(null)

  val (table, tableModel) = createTable

  setContentPane(content)

  addListeners()

  def createTable: (JTable, ParamTableModel) = {
    val tableModel = new ParamTableModel()
    val table = new JTable(tableModel)

    (table, tableModel)
  }

  def content: Container = {
    val panel = new JPanel(borderLayout)
    panel.setBorder(emptyBorder)
    panel.add(Toolbar(), BorderLayout.NORTH)
    panel.add(new JScrollPane(table), BorderLayout.CENTER)
    panel.add(DispatchPanel(), BorderLayout.SOUTH)
    panel
  }

  def addListeners() = {
    //TODO найти более короткий способ объявления и добавления слушателей событий
    EventBus.addListener(new Listener[AddParam] {
      override def handle(event: AddParam): Unit = {
        addParam(event.param)
      }
    }, classOf[AddParam])

    EventBus.addListener(new Listener[DeleteSelectedParam] {
      override def handle(event: DeleteSelectedParam): Unit = {
        val selectedRow = table.getSelectedRow
        if (selectedRow != -1) tableModel.removeRow(selectedRow)
      }
    }, classOf[DeleteSelectedParam])

    EventBus.addListener(new Listener[EditSelectedParam] {
      override def handle(event: EditSelectedParam): Unit = {
        val selectedRow = table.getSelectedRow
        if (selectedRow != -1) EventBus.fire(EditParam(tableModel.params(selectedRow)))
      }
    }, classOf[EditSelectedParam])

    EventBus.addListener(new Listener[Run] {
      override def handle(event: Run): Unit = {
        EventBus.fire(RunParamObserving(tableModel.params.toList))
      }
    }, classOf[Run])

    EventBus.addListener(new Listener[UpdateParam] {
      override def handle(event: UpdateParam): Unit = {
        println("updated: " + event.param)
        tableModel.params.find(p => event.param == p).get
        tableModel.params.foreach(p => println("in table: " + p))
        tableModel.fireTableChanged(null)
      }
    }, classOf[UpdateParam])
  }

  def addParam(param: Param): Unit = tableModel.addRow(param)
}

