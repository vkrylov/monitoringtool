package su.t1oo1.monitoring.ui

import su.t1oo1.monitoringtool.pluginapi.Plugin

/**
 * Модель редактора плагинов
 */
object PluginEditorModel {
  var plugins = Array.empty[Plugin]
}
