package su.t1oo1.monitoring.ui

import java.awt.BorderLayout
import javax.swing.{Box, JPanel}

import su.t1oo1.monitoring.ui.UiFactory._

/**
 * Тулбар главного окна
 */
object Toolbar {
  def apply(): Toolbar = {
    new Toolbar
  }
}

class Toolbar extends JPanel(borderLayout) {

  val presenter = new ToolbarPresenter(this)
  val panel = Box.createHorizontalBox
  val addButton = button("Add") { e =>
    presenter.onAdd()
  }
  panel.add(addButton)
  val deleteButton = button("Delete") { e =>
    presenter.onDelete()
  }
  panel.add(deleteButton)
  val editButton = button("Edit") { e =>
    presenter.onEdit()
  }
  panel.add(editButton)
  val pluginButton = button("Plugins") { e =>
    presenter.onPlugins()
  }
  panel.add(pluginButton)

  add(panel, BorderLayout.WEST)

  EventBus.addListener(new Listener[Run] {
    override def handle(event: Run): Unit = {
      enableEdit(b = false)
    }
  }, classOf[Run])

  EventBus.addListener(new Listener[Stop] {
    override def handle(event: Stop): Unit = {
      enableEdit(b = false)
    }
  }, classOf[Stop])

  def enableEdit(b: Boolean): Unit = {
    addButton.setEnabled(b)
    editButton.setEnabled(b)
    deleteButton.setEnabled(b)
  }
}
