package su.t1oo1.monitoring.ui

import javax.swing.table.DefaultTableModel

import su.t1oo1.monitoringtool.pluginapi.Param

/**
 * Модель таблицы параметров
 */
//FIXME починить обновление содержимого параметров
class ParamTableModel extends DefaultTableModel {
  val columns = Array[Object](
    "Name",
    "Type",
    "Config",
    "Periodicity",
    "Value")
  columns.foreach(addColumn(_))

  import scala.collection.mutable

  val params = mutable.ArrayBuffer[Param]()

  override def removeRow(row: Int) {
    super.removeRow(row)
    params -= params(row)
  }

  def addRow(param: Param) {
    params += param
    addRow(Array[Object](
      param.name,
      param.config.plugin.toString,
      param.config.asText,
      param.periodicity.toString,
      param.value.getOrElse("-")))
  }

  override def isCellEditable(row: Int, column: Int): Boolean = false
}
