package su.t1oo1.monitoring.ui

import java.awt._
import java.awt.event.{ActionEvent, ActionListener}
import javax.swing._

import su.t1oo1.monitoringtool.pluginapi.{ParamEditorPanel, Param, Plugin}

object ParamEditor {

  /**
   * @param parent родительский компонент
   * @param plugins доступные плагины
   * @return Редактор параметра
   */
  def apply(parent: Component, plugins: Array[Plugin]): ParamEditor = {
    val editor = new ParamEditor(parent: Component, new ParamEditorPresenter {
      override def onSave(param: Param): Boolean = {
        EventBus.fire(AddParam(param))
        true
      }

      override def onCancel(): Unit = {

      }

      override def onEdit(param: Param): Boolean = {
        EventBus.fire(UpdateParam(param))
        true
      }
    })
    editor add plugins
    editor
  }
}


/**
 * Редактор параметра. При выборе используемого типа мониторинга (а именно выборе используемого для
 * мониторинга плагина), специальная область будет установлена в зависимости от выбранного типа. По умолчанию
 * производится добавление параметра. Для редактирования параметра необходимо параметр привязать специальным методом
 *
 * @param parent родительский компонент
 * @param presenter презентер
 */
//FIXME убрать глюки спецобласти при смене типа мониторинга
//TODO необходим рефакторинг
class ParamEditor(parent: Component, presenter: ParamEditorPresenter) extends JDialog(SwingUtilities.getWindowAncestor(parent)) {
  import su.t1oo1.monitoring.ui.UiFactory._
  var paramEditor: Option[ParamEditorPanel] = None

  var param: Option[Param] = None

  setModal(true)
  setTitle("New parameter")
  setSize(new Dimension(400, 400))

  private  val content = new JPanel(borderLayout)
  content.setBorder(emptyBorder)

  private val fields = new JPanel(new GridLayout(3, 2, 5, 5))

  private val nameField = new TextField("New parameter name")
  fields.add(new Label("Name"))
  fields.add(nameField)

  private val typeFieldModel = new DefaultComboBoxModel[Plugin]()

  private val typeCombo = new JComboBox[Plugin](typeFieldModel)
  typeCombo.addActionListener(new ActionListener {
    override def actionPerformed(e: ActionEvent): Unit = {
      editor_(typeCombo.getSelectedItem.asInstanceOf[Plugin])
    }
  })
  fields.add(new Label("Type"))
  fields.add(typeCombo)

  private val periodicityField = new TextField("1000")
  fields.add(new Label("Periodicity, msec"))
  fields.add(periodicityField)

  content.add(fields, BorderLayout.NORTH)
  private val editorPanel = new JPanel(borderLayout)
  content.add(editorPanel, BorderLayout.CENTER)
  content.add(buttons, BorderLayout.SOUTH)

  setContentPane(content)
  setLocationRelativeTo(null)

  private def editor_(plugin: Plugin): Unit = {
    editorPanel.removeAll()
    val editor = plugin.editorPanel
    paramEditor = Some(editor)
    editorPanel.add(editor, BorderLayout.NORTH)
    editor.config
    repaint()
  }

  def add(plugins: Array[Plugin]): Unit = plugins.foreach(typeFieldModel.addElement)

  private def buttons = {
    val panel = Box.createHorizontalBox()
    val saveButton = button("Save") { e =>
      param match {
        case None => presenter.onSave(createParam)
        case Some(p) => p.name = nameField.getText
          p.config = paramEditor.get.config
          p.value = None
          p.periodicity = periodicityField.getText.toInt
          presenter.onEdit(p)
      }

      dispose()
    }
    panel.add(saveButton)
    val cancelButton = button("Cancel") { e =>
      presenter.onCancel()
      dispose()
    }
    panel.add(cancelButton)
    panel
  }

  /**
   * Привязать к редактору параметр. После этого редактор будет редактировать параметр, а не сохранять как новый
   *
   * @param p редактируемый параметр
   */
  def bind(p: Param): Unit = {
    setTitle("Edit param")
    param = Some(p)
    nameField.setText(p.name)
    for (i <- 0 until typeCombo.getItemCount) {
      val plugin = typeCombo.getItemAt(i)
      if (plugin == p.config.plugin) {
        typeCombo.setSelectedItem(plugin)
      }
    }
    periodicityField.setText(p.periodicity.toString)
  }

  def createParam(): Param = {
    Param(nameField.getText, paramEditor.get.config, None, periodicityField.getText.toInt)
  }

}
