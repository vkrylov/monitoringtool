package su.t1oo1.monitoring

import java.util.concurrent._

import su.t1oo1.monitoring.ui.{Stop, EventBus, Listener, RunParamObserving}
import su.t1oo1.monitoringtool.pluginapi.Param

/**
 * Объект работы с запуском заданий. Работает через уведомления с помощью сообщений
 */
object TaskExecutor {
  private var scheduledThreadPool = Executors.newScheduledThreadPool(5)

  EventBus.addListener(new Listener[RunParamObserving] {
    override def handle(event: RunParamObserving): Unit = {
      execute(event.params)
    }
  }, classOf[RunParamObserving])

  EventBus.addListener(new Listener[Stop] {
    override def handle(event: Stop): Unit = {
      stopAll()
    }
  }, classOf[Stop])

  def execute(params: List[Param]): Unit = {
    for (p <- params) {
      val task: Runnable = p.config.createTask(p)
      scheduledThreadPool.scheduleAtFixedRate(task, 0, p.periodicity, TimeUnit.MILLISECONDS)
    }
  }

  def stopAll(): Unit = {
    //TODO пересмотреть перезапуск заданий
    scheduledThreadPool.shutdown()
    if (scheduledThreadPool.isShutdown) {
      println("All tasks has been stopped")
    } else {
      println("scheduledThreadPool is still working")
    }
    scheduledThreadPool = Executors.newScheduledThreadPool(5)
  }
}